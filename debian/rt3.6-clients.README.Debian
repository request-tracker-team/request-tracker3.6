Notes for the Debian package rt3.6-clients.

RT COMMAND-LINE TOOL
--------------------

The file /etc/request-tracker3.6/rt.conf should be edited to reflect the
server URL.


HTTPS
-----

If you want mailgate or the RT CLI to talk HTTPS to the server rather
than plain HTTP, you need libio-socket-ssl-perl.


MAIL CONFIGURATION
------------------

Configure the mail gateway. You may need to cause your MTA to run the
aliases. Add the following lines to the mail aliases (/etc/aliases):

rt:         "|/usr/bin/rt-mailgate --queue General --action correspond --url <url>"
rt-comment: "|/usr/bin/rt-mailgate --queue General --action comment --url <url>"

where <url> is the base URL of your RT system,
e.g. https://rt.example.com/ or http://www.example.com/rt/.

If you want your URL to be HTTPS, you need to install
libio-socket-ssl-perl too.

If you are using exim4 you will probably need to manually turn on pipe
support. There are two options
  1- (using exim split config)
  2- (using exim monolithic config)

(1) Create a file:

  # echo "SYSTEM_ALIASES_PIPE_TRANSPORT = address_pipe" >> /etc/exim4/conf.d/main/90_exim4-config_requesttracker

(2) Do this:

  Edit the file /etc/exim4/exim4.conf.template and find the line "begin acl"
  before this line, add:

.ifndef SYSTEM_ALIASES_PIPE_TRANSPORT
SYSTEM_ALIASES_PIPE_TRANSPORT = address_pipe
.endif

After both (1) and (2), you need to update and reload exim4:

  # /usr/sbin/update-exim4.conf
  # /etc/init.d/exim4 reload

If you are using Sendmail you will need to add rt-mailgate into the
/etc/mail/smrsh/ directory. Do something like:

ln -s /usr/bin/rt-mailgate /etc/mail/smrsh/rt-mailgate

The smrsh directory contains all the binaries that Sendmail is
permitted to run via the '|program' syntax in order to improve the
over all security of your system. See smrsh(8) for more details.

Configuration hints for other MTAs are warmly appreciated! (Please
file wishlist bug-reports.)

DEBUGGING DEFERRED DELIVERIES
-----------------------------

If your mails are just getting deferred with temporary failures, running
rt-mailgate from the command line may help to pinpoint the problem. An
example where libio-socket-ssl-perl is missing:

  % echo 'From: <root@localhost>' | rt-mailgate --queue general --action correspond --url https://localhost/rt/
  An Error Occurred
  =================
  
  501 Protocol scheme 'https' is not supported
  (Crypt::SSLeay not installed)


This file was originally written by 

Stephen Quinney <sjq@debian.org>, Thu Sep 15 15:23:30 2005

 -- Niko Tyni <ntyni@iki.fi> Mon, 27 Nov 2006 22:57:00 +0200

