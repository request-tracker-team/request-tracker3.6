.TH update-rt-siteconfig-3.6 8
.SH NAME
update-rt-siteconfig-3.6 \- update the Request Tracker configuration file
.SH SYNOPSIS
.B update-rt-siteconfig-3.6
.SH DESCRIPTION
This program will generate the main Request Tracker configuration
file, \fI/etc/request-tracker3.6/RT_SiteConfig.pm\fR,
by concatenating configuration snippets in the directory
\fI/etc/request-tracker3.6/RT_SiteConfig.d/\fR.

The resulting file is managed with \fIucf\fR, so that local modifications
are not overwritten without permission from the user.

The program ignores configuration snippets named *.dpkg-* and *.ucf-*,
which are standard names for special files managed by \fIdpkg\fR and
\fIucf\fR.

If the resulting file already exists, the program will not modify
its owner or permission bits. If it does not exist yet, it will
be created with permissions 0600.
.SH OPTIONS
No options are currently supported.
.SH FILES
.br
.nf
\fI/etc/request-tracker3.6/RT_SiteConfig.d/\fR \- directory for configuration file snippets
\fI/etc/request-tracker3.6/RT_SiteConfig.pm\fR \- the actual configuration file
.SH BUGS
None known.
.SH "SEE ALSO"
.BR ucf(1)
,
.BR dpkg(1)
.SH AUTHOR
Niko Tyni <ntyni@iki.fi>
.br
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.  You may also
redistribute it and/or modify it under the terms of the Perl
Artistic License.

