#! /usr/bin/perl
#
# add-whatis: Add a short description to POD entry for whatis parsing
#
#   -- Andrew Stribblehill <ads@debian.org> 2003-04-10

use warnings;
use strict;
use Pod::Man;
use IO::File;

{ package WhatisPreprocessor;

  # this is needed for Perl 5.10
  use overload '<>' => \&getline, fallback => 1;

  sub new {
      my ($self, $filename, $module, $desc) = @_;
      my $fh = new IO::File("<$filename") or return 0;
      return bless { state => "_pre", input => $fh,
		     module => $module, desc => $desc}, $self;
  }

  sub getline {
      my $self = shift;
      my $fh = $self->{input};

      unless ($self->{pause}) {
	  $_ = $fh->getline;
      } else {
	  $_ = $self->{pause};
      }

      return undef if (!defined $_);

      return $_ if /^\s*$/;
      # Terminal states don't begin with _
      #return $_ if ($self->{state} !~ /^_/);
      
    BLOCK: {
	if ($self->{state} eq "_pre") {
	    if (/^=head1 NAME/) {
		$self->{state} = "_pad";
		$self->{pause} = $_;
		$_ = "\n"; # Euw
	    }
	} elsif ($self->{state} eq "_pad") {
	    $self->{pause} = 0;
	    $self->{state} = "_head1";
	} elsif ($self->{state} eq "_head1") {
	    if (/(\S+)\s+-+\s+(.*)/) {
		$self->{state} = "tidied"; $_="$1 - $2\n";
		$self->{whatis} = chomp;
	    }
	    if (/(\S+)$/) {$self->{state} = "_active"; redo;}
	} elsif ($self->{state} eq "_active") {
	    if ($self->{desc}) {
		$self->{state} = "overwrote";
		$_ = "$self->{module} - $self->{desc}\n";
		$self->{whatis} = $_;
	    } else {
		$self->{state} = "default";
		$_ = "$self->{module} - Request Tracker internal module\n";
		$self->{whatis} = $_;
	    }
	}
    }
#      print $_;
      return $_;
  }

  sub bad_manpage {
      return ($_[0]->{state} =~ m/^_/);
  }
     
  sub close { 
      close $_[0]->{input};
  }
}

sub file2name {
    $_ = shift;
    if (s/\.pm$//) {
	# Special module code
	s(^lib/)();
	s(/)(::)g;
    } else {
	s(^.*/)();
    }
    return $_;
}

while (<>) {
    chomp;
    my ($file, $desc) = split /\s+/, $_, 2;
    my $mansect = "3pm";
    ($file, $mansect) = ($1, $2) if $file =~ m/^(\S+)\((\w+)\)$/;
    my $name = file2name($file);
    my $man = "man/$name.$mansect";
    open MAN, ">$man";

    my $filter = WhatisPreprocessor->new($file, $name, $desc);
    my $pod = Pod::Man->new(section => $mansect,
			    center => "Request Tracker Reference",
			    name => $name);
    $pod->parse_from_filehandle($filter, \*MAN);
    print while ($filter->getline);
    #print STDERR "$file -> $man: $filter->{state}\n";
    if ($filter->bad_manpage) {
	print STDERR " Bad manpage $man: deleting!\n";
	unlink $man;
    } else {
	print STDERR "$filter->{whatis}";
    }
    $filter->close;
}
