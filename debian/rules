#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpatch/dpatch.make

VER=3.6

DEBIAN=$(CURDIR)/debian

RT3 = request-tracker$(VER)
CLIENTS = rt$(VER)-clients

RT3_PKG = $(DEBIAN)/$(RT3)
CLIENTS_PKG = $(DEBIAN)/$(CLIENTS)

RT3_BUGDIR = $(RT3_PKG)/usr/share/bug/$(RT3)
CLIENTS_BUGDIR = $(CLIENTS_PKG)/usr/share/bug/$(CLIENTS)

build: patch build-stamp

build-stamp: configure
	dh_testdir

	# Although there's a configure stage, we have no make step.

	chmod u+x ./configure
	./configure \
	  --enable-layout=Debian \
	  --with-rt-group=root \
	  --with-web-user=www-data \
	  --with-web-group=www-data \
	  --with-libs-group=root \
	  --with-db-type=Pg \
	  --with-db-dba=postgres \
	  --with-speedycgi=/usr/bin/speedy

	mkdir man
	perl debian/scripts/fix-whatis < debian/whatis

	touch build-stamp

clean: unpatch
	dh_testdir
	dh_testroot
	rm -f build-stamp 
	rm -f config.log config.status config.pld
	# Remove automatically generated files
	find . -name '*.in' -print | perl -ne 'chomp; s/\.in$$//; unlink'
	rm -rf man
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	$(MAKE) install DESTDIR=$(RT3_PKG)
	rm -rf $(RT3_PKG)/usr/share/$(RT3)/lib/t/
	rm -rf $(RT3_PKG)/usr/bin/mason_handler.svc
	rm -rf $(RT3_PKG)/usr/sbin/rt-test-dependencies

	# all the files inside are zero-sized as of 3.6.6
	# this is designed to get skipped if that ever changes
	[ 0 != $$(find $(RT3_PKG)/usr/share/$(RT3)/html/NoAuth/js/scriptaculous \
	        -type f -size +0 | wc -l) ] || \
		$(RM) -r $(RT3_PKG)/usr/share/$(RT3)/html/NoAuth/js/scriptaculous

	for x in webmux.pl mason_handler.fcgi mason_handler.scgi; do \
	  mv $(RT3_PKG)/usr/bin/$$x $(RT3_PKG)/usr/share/$(RT3)/libexec/; \
	done
#	mv $(RT3_PKG)/usr/bin/standalone_httpd \
#           $(RT3_PKG)/usr/bin/rt-standalone_httpd
	rm $(RT3_PKG)/usr/bin/standalone_httpd
	dh_install


test:
	perl debian/scripts/check-deps

# Build architecture-dependent files here.
binary-arch: build install
# We have nothing to do by default.

# Build architecture-independent files here.
binary-indep: build install 
	dh_testdir
	dh_testroot
	dh_usrlocal
	dh_installchangelogs
	# docs/Security is obsolete
	dh_installdocs -Xdocs/Security
	dh_installexamples bin/mason_handler.svc bin/standalone_httpd
	dh_installman man/*
	dh_installdebconf
	dh_installlogcheck
	cd $(RT3_PKG) && perl $(DEBIAN)/scripts/move-alts $(VER) \
	    $(DEBIAN)/alts $(DEBIAN)/$(CLIENTS).alts
	dh_movefiles --sourcedir=debian/$(RT3)
	dh_link
	dh_fixperms
	find $(RT3_PKG)/etc -type f -print0 | xargs -0 chmod a-x
	# Fix permissions for Mason files
	find $(RT3_PKG)/usr/share/$(RT3)/html -type f -print0 | xargs -0 --no-run-if-empty chmod 0644
	# Ensure that the stuff in libexec is executable by everyone
	find $(RT3_PKG)/usr/share/$(RT3)/libexec -type f -print0 | xargs -0 --no-run-if-empty chmod 0755
	# Remove any leftover .in files
	find $(RT3_PKG)/usr/share/$(RT3)/lib -type f -name '*.in' -print0 | xargs -0 --no-run-if-empty rm 
	# Fix permissions for Perl modules
	find $(RT3_PKG)/usr/share/$(RT3)/lib  -type f -print0 | xargs -0 --no-run-if-empty chmod 0644

	# This is generated by the maintainer scripts
	$(RM) $(RT3_PKG)/etc/request-tracker3.6/RT_SiteConfig.pm

	install -m 644 debian/scripts/siteconfig.template $(RT3_PKG)/usr/share/request-tracker3.6/debian/
	install -m 644 debian/scripts/dbconfig.template $(RT3_PKG)/usr/share/request-tracker3.6/debian/
	install -m 755 debian/scripts/write-siteconfig $(RT3_PKG)/usr/share/request-tracker3.6/debian/
	install -m 755 debian/scripts/fix-website-in-database $(RT3_PKG)/usr/share/request-tracker3.6/debian/
	install -m 755 debian/scripts/update-rt-siteconfig $(RT3_PKG)/usr/sbin/update-rt-siteconfig-3.6
	install -m 644 debian/scripts/update-rt-siteconfig.man $(RT3_PKG)/usr/share/man/man8/update-rt-siteconfig-3.6.8

	install -m 644 debian/lintian-overrides $(RT3_PKG)/usr/share/lintian/overrides/$(RT3)

	# dh_link already made the pgsql and sqlite links
	install -m 755 debian/scripts/dbconfig-install $(RT3_PKG)/usr/share/dbconfig-common/scripts/request-tracker3.6/install/mysql

	# note that this needs dbconfig-common (>= 1.8.38), see #472944
	for dbtype in mysql pgsql sqlite3; do \
		dh_link usr/share/request-tracker3.6/debian/fix-website-in-database \
		        usr/share/dbconfig-common/scripts/request-tracker3.6/upgrade/$${dbtype}/3.6.6-2~experimental1; \
	done

	chmod 755 $(RT3_PKG)/usr/share/bug/$(RT3)/script
	find $(RT3_PKG)/var/cache/$(RT3)/ -type d -print0 | xargs --null chown www-data:root
	find $(RT3_PKG)/var/cache/$(RT3)/ -type d -print0 | xargs --null chmod 2750

	chown www-data:www-data $(RT3_PKG)/var/log/$(RT3)/
	chmod 2755 $(RT3_PKG)/var/log/$(RT3)/
	dh_compress
	dh_perl
	dh_usrlocal
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install test
